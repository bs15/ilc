import colors from 'vuetify/es5/util/colors'
// import Lightbox from 'vue-my-photos'
// Vue.component('lightbox', Lightbox);

export default {
  server: {
    port:8000,
    host:'0.0.0.0'
  },
  mode: 'universal',
  /*
  ** Headers of the page
  */
  router: {
    base: '/',
    routeNameSplitter: '/'
  },
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel: "stylesheet", type: "text/css", href: "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Raleway&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Merriweather&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Lato&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Lato|Montserrat&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Great+Vibes&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/icon?family=Material+Icons" },
      { rel:"stylesheet", href: "https://fonts.googleapis.com/css?family=Lato&display=swap"},
      { rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"},
      { rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"},
      // { rel: "stylesheet", href: "https://unpkg.com/vue-gallery-slideshow"},
      { rel: "stylesheet", type: "text/css", href: "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Raleway&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Merriweather&display=swap" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/icon?family=Material+Icons" }
      // { rel: "stylesheet", href="https://fonts.googleapis.com/icon?family=Material+Icons" }
      // { rel: "stylesheet", href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"},
      // { rel: "stylesheet", href: "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",  integrity: "sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN", crossorigin: "anonymous"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
 loading: {  color: '#3B8070' },
  /*
  ** Global CSS
  */
  css: [
    '~assets/css/main.css',
    'vue-plyr/dist/vue-plyr.css'

  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-plyr',
    // '~/plugins/vue-youtube',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    'vue-scrollto/nuxt',
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
